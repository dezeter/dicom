
<?php
$this->group([
    'namespace' => 'ClientBase'
],function($route){
    //$route->screen('clientbase/clients/create', 'Patient\PatientEdit','dashboard.clinic.patient.create');
    $route->screen('clientbase/clients/{client}/edit', 'Clients\ClientsEdit','dashboard.clientbase.clients.edit');
    $route->screen('clientbase/clients', 'Clients\ClientsList','dashboard.clientbase.clients.list');
});